# Resume and Curriculum Vitae

A latex based single column resume (single page) and curriculum vitae (multiple page). I have used tables extensively in place of bullets (personal preference). Feel free to use the formats, make sure to follow the project!

## Resume Preview

![Resume Screenshot](/resume.png)

## Curriculum Vitae

![CV Screenshot](/cv.png)

## License

MIT License

## Acknowledgements

Sourabh Bajaj's resume : [Sourabh Bajaj](https://github.com/sb2nov)
