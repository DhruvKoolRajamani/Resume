# Resume

A latex based, single column resume for Developers.

## Preview

![Resume Screenshot](/resume.png)

## License

MIT License

## Acknowledgements

Sourabh Bajaj's resume : [Sourabh Bajaj](https://github.com/sb2nov)
